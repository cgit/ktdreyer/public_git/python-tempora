%global pkgname tempora

Name:           python-%{pkgname}
Version:        1.14
Release:        1%{?dist}
Summary:        Objects and routines pertaining to date and time
License:        MIT
Source0:        https://files.pythonhosted.org/packages/source/t/%{pkgname}/%{pkgname}-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python3-devel
# Drop?
# BuildRequires:  python3-backports-unittest_mock
BuildRequires:  python3-freezegun
BuildRequires:  python3-jaraco-functools >= 1.20
# Probably jaraco.packaging is not a hard requirement
# BuildRequires:  python3dist(jaraco.packaging) >= 3.2
BuildRequires:  python3-pytest >= 3.5
BuildRequires:  python3-pytz
BuildRequires:  python3-setuptools
BuildRequires:  python3-setuptools_scm >= 1.15.0
BuildRequires:  python3-six
# Is sphinx a hard requirement?
# BuildRequires:  python3-sphinx

%global _description\
Objects and routines pertaining to date and time.

%description %_description

%package -n python3-%{pkgname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pkgname}}
%description -n python3-%{pkgname} %_description

%prep
%autosetup -n %{pkgname}-%{version}

sed -i 's/ --flake8//' pytest.ini

%build
SETUPTOOLS_SCM_PRETEND_VERSION=%{version}
%{py3_build}

%install
%{py3_install}

%check
py.test-%{python3_version} -v

%files -n python3-%{pkgname}
%license LICENSE
%doc README.rst
%{python3_sitelib}/*
%{_bindir}/calc-prorate

%changelog
* Mon Feb 25 2019 Ken Dreyer <kdreyer@redhat.com> - 1.14-1
- Initial package.
